#!/bin/bash -eu
set -o pipefail

ROOT_DIR=$(dirname "$(readlink -f "$0")")
export ROOT_DIR

echo "=== Updating source..."
git submodule update --remote --rebase

cd src/

echo "=== Updating feeds..."
./scripts/feeds update -a
./scripts/feeds install -a

echo "=== Generating configuration..."
cat "${ROOT_DIR}/base.conf" "${ROOT_DIR}/targets/${1}.conf" | envsubst > .config
make defconfig

echo "=== Downloading deps..."
make -j8 download

echo "=== Building with $(nproc) workers..."
make "-j$(nproc)"
